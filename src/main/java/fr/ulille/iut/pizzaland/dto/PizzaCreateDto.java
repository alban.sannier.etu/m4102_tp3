package fr.ulille.iut.pizzaland.dto;

import java.util.UUID;

public class PizzaCreateDto {
	private String name;

	public PizzaCreateDto() {}
		
	public void setName(String name) {
		this.name = name;
	}
 		
	public String getName() {
		return name;
	}
}