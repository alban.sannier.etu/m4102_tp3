package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Produces("application/json")
@Path("/pizzas")
public class PizzaRessourceTest extends JerseyTest{
	private PizzaDao dao;
	private IngredientDao idao;
	  
	  @Override
	  protected Application configure() {
	     BDDFactory.setJdbiForTests();

	     return new ApiV1();
	  }
		
	  @Before
	  public void setEnvUp() {
	    dao = BDDFactory.buildDao(PizzaDao.class);
	    idao =BDDFactory.buildDao(IngredientDao.class);
	    idao.createTable();
	    dao.createTableAndIngredientAssociation();
	  }

	  @After
	  public void tearEnvDown() throws Exception {
		 idao.dropTable();
		 dao.dropTableAndIngredientAssociation();
	  }
	  
	  @Test
	  public void testGetEmptyListPizza() {
		  Response response = target("/pizzas").request().get();
		  assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		  List<PizzaDto> ingredients;
		  ingredients = response.readEntity(new GenericType<List<PizzaDto>>(){});
		  assertEquals(0, ingredients.size());

	  }
	  @Test
	  public void testGetExistingPizza() {
	    Pizza p=new Pizza();
	    p.setName("Espagnole");
	    dao.insert(p);

	    Response response = target("/pizzas").path(p.getId().toString()).request(MediaType.APPLICATION_JSON).get();
 
	    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	    Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
	    assertEquals(p, result);
	  }
	  @Test
	  public void testGetNotExistingPizza() {
	    Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
	    assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	  }
	  @Test
	    public void testCreatePizza() {
	        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
	        pizzaCreateDto.setName("Chorizo");

	        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

	        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

	        PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

	        assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
	        assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
	    }
	  @Test
	    public void testCreateSamePizza() {
	        Pizza pizza = new Pizza();
	        pizza.setName("Chorizo");
	        dao.insert(pizza);

	        PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
	        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

	        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	    }
	  @Test
	    public void testCreatePizzaWithoutName() {
	        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

	        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

	        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	    }
	  @Test
	    public void testDeleteExistingPizza() {
	      Pizza pizza = new Pizza();
	      pizza.setName("Chorizo");
	      dao.insert(pizza);

	      Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();

	      assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

	      Pizza result = dao.findById(pizza.getId());
	      assertEquals(result, null);
	   }
	  @Test
	   public void testDeleteNotExistingPizza() {
	     Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
	     assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	   }
	  @Test
	   public void testGetPizzaName() {
	     Pizza pizza = new Pizza();
	     pizza.setName("Chorizo");
	     dao.insert(pizza);

	     Response response = target("pizzas").path(pizza.getId().toString()).path("name").request().get();

	     assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	     assertEquals("Chorizo", response.readEntity(String.class));
	  }
	  @Test
	  public void testGetNotExistingPizzaName() {
	    Response response = target("pizza").path(UUID.randomUUID().toString()).path("name").request().get();

	    assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	  }
	  @Test
	  public void testGetExistingPizzaWithIngredients() {
	    Pizza p=new Pizza();
	    p.setName("Espagnole");
	    Ingredient chorizo=new Ingredient();
	    chorizo.setName("Chorizo");
	    idao.insert(chorizo);
	    List<Ingredient> ingredient=new ArrayList<>();
	    ingredient.add(chorizo);
	    p.setIngredients(ingredient);
	    dao.insertTablePizzaAndIngredientAssociation(p);
	    
	    Response response = target("/pizzas").path(p.getId().toString()).request(MediaType.APPLICATION_JSON).get();
 
	    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	    Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
	    assertEquals(p, result);
	  }
}