|1|2|3|4|
|-|-|-|-|
| /pizza         | GET         | <-application/json<br><-application/xml                      |                   | liste des pizzas (P2)         |
| /pizza/{id}    | GET         | <-application/json<br><-application/xml                      |                   | une pizza (P2) ou 404         |
| /pizz/{id}/name| GET         | <-text/plain                                                 |                   | le nom de le pizza ou 404     | 
| /pizz/{id}/name| GET         | <-text/plain                                                 |                   | la liste d'ingrédients ou 404 |
| /pizza         | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza(P1)         | Nouvelle pizza(P2)<br>409 si la pizza existe déjà (même nom) |
| /pizza/{id}    | DELETE      |                                                              |                   |                               |


Une pizza contient une liste d'ingrédients, un id et un nom. La
représentation JSON est par exemple :

    {
    "[{"id": "ac84151b-8c41-57b7-657c-d4963a471a44",
      "name": "mozzarella"}],
      "id": "ac84151b-8c41-57b7-657c-d4963a471a44",
      "name": "Reine"
    }

A la création, l'id est générée par JavaBean si il n'est pas connu . 
 la représentation JSON est composé du nom et de la liste des ingrédients.


    { 
    "[{"id": "a785f364-75c7-47e1-3f48-8567e4c3f47a",
      "name": "mozzarella"}],
    "name": "Reine" 
    }
